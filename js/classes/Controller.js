class Controller {
    constructor(insurance, ui) {
        this.insurance = insurance;
        this.ui = ui;
    }

    unitApp() {
        addEventListener('DOMContentLoaded', () => {
            this.ui.fillSelectYear();
        });

        this.sendForm();
    }

    sendForm() {
        this.ui.form.addEventListener('submit', (e) => {
            e.preventDefault();
            
            // 1. Get the field input data
            const data = this.ui.getData();

            // 2. validate data is empty 
            if( data.length === 0 ) {
                return;
            }

            // 3. showing success message cotizando
            this.ui.showMessage('cotizando...', 'success');

            // 4. fill object insurance with form  data
            this.insurance.brand = data.brand;
            this.insurance.year = data.year;
            this.insurance.type = data.type;

            // 5. calculate the insurance
            this.insurance.quote();

            // 6. add data to UI
            this.ui.showResult(this.insurance);

        });
    }
}

export default Controller;
