
class UI {
    constructor() {
        this.form = document.querySelector('#quote-insurance');
    }

    getData() {
        let data = {
            'brand': document.querySelector('#brand').value,
            'year': document.querySelector('#year').value,
            'type': document.querySelector('input[name="type"]:checked').value
        }

        return this.validateData(data);
    }    

    fillSelectYear() {
        const   max = new Date().getFullYear(),
                min = max - 20;

        const selectYear = document.querySelector('#year');

        for(let i = max; i >= min; i--) {
            let option = document.createElement('option');
            option.value = i;
            option.textContent = i;
            selectYear.appendChild(option);
        }
    }

    showMessage(msg, type) {

        const alerts = document.querySelector('.alert');
        if(alerts === null) {
            const div = document.createElement('div');
            div.classList.add('alert', 'mt-10');

            if(type === 'error') {
                div.classList.add('error');
            } else {
                div.classList.add('sucess');
            }

            div.textContent = msg;

            const form = document.querySelector('#quote-insurance');
            form.insertBefore(div, document.querySelector('#result'));

            setTimeout(function(){
                div.remove();
            }, 3000);
            
        }
    }

    showResult(insurance) {

        if(document.querySelector('#result div') !== null) {
            document.querySelector('#result div').remove();
        }

        const div = document.createElement('div');
        div.classList.add('mt-10');

        div.innerHTML = `
            <p class="header">Tu resumen</p>
            <p class="font-bold">Total: <span class="font-normal">${insurance.totalPrice}</span></p>
            <p class="font-bold">Marca: <span class="font-normal capitalize">${insurance.brand}</span></p>
            <p class="font-bold">Año: <span class="font-normal">${insurance.year}</span></p>
            <p class="font-bold">Tipo: <span class="font-normal">${insurance.type}</span></p>
        `;

        const containerResult = document.querySelector('#result');
        containerResult.appendChild(div);
        containerResult.style.display = 'none';
    
        const spinner = document.querySelector('#loading');
        spinner.style.display = 'block';

        setTimeout(function(){
            spinner.style.display = 'none';
            containerResult.style.display = 'block';
        }, 3000);
    }

    isEmpty(el) {
        return el === '';
    }

    validateData(data) {
        for( let key in data ) {
            if ( this.isEmpty(data[key]) ) {
                this.showMessage('El campo es obligatorio', 'error');
                data = [];
                return data;
            }
        }

        return data;
    }
}

export default UI;
