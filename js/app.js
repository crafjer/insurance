import Controller from "./classes/Controller.js";
import Insurance from "./classes/Insurance.js";
import UI from "./classes/UI.js";

const ui = new UI();
const insurance = new Insurance();
const controller = new Controller(insurance, ui);

controller.unitApp();
